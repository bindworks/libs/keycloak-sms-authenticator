package eu.bindworks.keycloak.smsotp.services.webhook;

import com.fasterxml.jackson.core.io.JsonStringEncoder;
import eu.bindworks.keycloak.smsotp.services.SmsService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.jboss.logging.Logger;
import org.keycloak.common.util.Base64;
import org.keycloak.connections.httpclient.HttpClientProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.utils.StringUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class WebHookSMSService implements SmsService {

    private static final Logger logger = Logger.getLogger(WebHookSMSService.class);

    private final KeycloakSession session;
    private final String username;
    private final String password;
    private final String url;
    private final String template;

    public WebHookSMSService(KeycloakSession session, String username, String password, String url, String template) {
        this.session = session;
        this.username = username;
        this.password = password;
        this.url = url;
        this.template = template;
    }

    @Override
    public void sendSms(String phoneNumber, String message) {
        try {
            CloseableHttpClient httpClient = session.getProvider(HttpClientProvider.class).getHttpClient();
            // httpClient NOT to be closed as per https://www.keycloak.org/docs-api/21.0.2/javadocs/org/keycloak/connections/httpclient/HttpClientProvider.html
            HttpPost httpPost = new HttpPost(url);

            if (StringUtil.isNotBlank(username) && StringUtil.isBlank(password)) {
                final String basicCredentials = String.format("%s:%s", username, password);
                httpPost.setHeader("Authorization", "Basic " + Base64.encodeBytes(basicCredentials.getBytes()));
            } else if (StringUtil.isNotBlank(username)) {
                httpPost.setHeader("Authorization", username);
            }

            if (template.startsWith("{")) {
                String substitutedTemplate = template
                        .replace("*PHONENUMBER*", new String(JsonStringEncoder.getInstance().quoteAsString(phoneNumber)))
                        .replace("*MESSAGE*", new String(JsonStringEncoder.getInstance().quoteAsString(message)));
                httpPost.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
                httpPost.setEntity(new StringEntity(substitutedTemplate, ContentType.APPLICATION_JSON));
            } else {
                String substitutedTemplate = template
                        .replace("*PHONENUMBER*", URLEncoder.encode(phoneNumber, StandardCharsets.UTF_8))
                        .replace("*MESSAGE*", URLEncoder.encode(message, StandardCharsets.UTF_8));
                httpPost.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
                httpPost.setEntity(new StringEntity(substitutedTemplate, ContentType.APPLICATION_FORM_URLENCODED));
            }

            logger.tracef("Sending SMS message number %s: '%s'...", phoneNumber, template);

            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                // Objects created via httpClient need to be closed properly as per https://www.keycloak.org/docs-api/21.0.2/javadocs/org/keycloak/connections/httpclient/HttpClientProvider.html
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    entity.writeTo(outputStream);
                } else {
                    outputStream.writeBytes("*EMPTY*".getBytes(StandardCharsets.UTF_8));
                }
                String responseString = "Status: " + response.getStatusLine() + ", Response: " + outputStream.toString(StandardCharsets.UTF_8);

                if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() < 300) {
                    logger.tracef("SMS message sent to number %s: '%s' with %s...", phoneNumber, template, responseString);
                } else {
                    logger.error(responseString);
                    throw new RuntimeException("SMS server responded with " + responseString);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
